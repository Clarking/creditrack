
export class Task {
    name: string;
    type: number;
    user: string;
    due_date: Date;
    status: string;

    constructor(values: Object = {}){
      Object.assign(this, values);
    }
  }
  