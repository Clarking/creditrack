import { Component, OnInit } from '@angular/core';
import { Task } from '../models/task';
import { Tag } from '../models/tag';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-tasks-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class TaskManageComponent implements OnInit {
  private tasks:Task[];
  private task:Task;
  private take:number = 15;
  private skip:number = 0;
  private tags: Tag[];

  constructor(private taskService: TasksService) {

  }

  ngOnInit() {

    this.task = <Task>{};

    this.taskService.getTasks(this.take, this.skip)
    .subscribe( 
      (data: any) => this.tasks =data,
              err => console.log(err.error));

  }

  addTask() {

  }

  editTask() {
    
  }

  removeTask() {

  }

  assignTask() {

  }

  getTaskTags(){

  }

  addTag(){

  }

  removeTag(){

  }

  getTasks() {

  }

}
