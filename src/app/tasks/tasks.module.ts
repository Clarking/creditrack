import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskManageComponent } from './manage/manage.component';
import { taskRoutes } from './task.routes';
import { TasksService } from './tasks.service';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TaskAddComponent } from './add/add.component';
import { TasksMenuComponent } from './menu/menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    RouterModule.forChild(taskRoutes),
  ],
  declarations: [
    TaskManageComponent, 
    TaskAddComponent, 
    TasksMenuComponent 
  ],  
  providers: [TasksService]
})
export class TasksModule { }
