import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Task } from './models/task';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from '../services/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  private tasks: Task[];
  private tasks_url: string;
  private httpOptions: any = {};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private errorHandler: ErrorHandlerService) {

    this.tasks_url = this.authService.endpoint("tasks");
    this.httpOptions.headers =  this.authService.getHeaders();
  }

  getTasks(take: number, skip: number) {
    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('take', take);
    this.httpOptions.params.append('skip', skip);

    return this.http.get<Task[]>(this.tasks_url, this.httpOptions);
  }

  getTask(id: number) {

    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('id', id);
    return this.http.get<Task[]>(this.tasks_url, this.httpOptions);
  }

  saveTask(task: Task) {
    return this.http.post<Task>(
      this.tasks_url,
      task,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('addTask'))
      );
  }

  updateTask(task: Task) {
    return this.http.put<Task>(
      this.tasks_url,
      task,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('addTask'))
      );
  }  

  deleteTask(id: number) {

    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('id', id.toString());

    return this.http.delete<Task>(
      this.tasks_url,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('addTask'))
      );
  }
}
