import { Routes } from '@angular/router';
import { TaskManageComponent } from './manage/manage.component';
import { TaskAddComponent } from './add/add.component';
import { AuthGuard } from '../guards/auth.guard';

export const taskRoutes: Routes = [
  {path: 'add', component: TaskAddComponent, canActivate: [AuthGuard]},
  {path: '', component: TaskManageComponent, canActivate: [AuthGuard]}
];
