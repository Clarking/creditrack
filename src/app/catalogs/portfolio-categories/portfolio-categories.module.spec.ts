import { PortfolioCategoriesModule } from './portfolio-categories.module';

describe('PortfolioCategoriesModule', () => {
  let portfolioCategoriesModule: PortfolioCategoriesModule;

  beforeEach(() => {
    portfolioCategoriesModule = new PortfolioCategoriesModule();
  });

  it('should create an instance', () => {
    expect(portfolioCategoriesModule).toBeTruthy();
  });
});
