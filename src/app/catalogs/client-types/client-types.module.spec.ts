import { ClientTypesModule } from './client-types.module';

describe('ClientTypesModule', () => {
  let clientTypesModule: ClientTypesModule;

  beforeEach(() => {
    clientTypesModule = new ClientTypesModule();
  });

  it('should create an instance', () => {
    expect(clientTypesModule).toBeTruthy();
  });
});
