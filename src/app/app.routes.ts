import { Routes } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';

export const appRoutes: Routes = [
 
  {path: 'tasks', loadChildren: './tasks/tasks.module#TasksModule'},
  {path: 'clients', loadChildren : './clients/clients.module#ClientsModule'},
  {path: 'portfolios', loadChildren : './portfolios/portfolios.module#PortfoliosModule' },
  {path: 'config', loadChildren : './config/config.module#ConfigModule' },
  {path: 'login', component: LoginComponent}, 
  {path: '', redirectTo : '/tasks', pathMatch: 'full'}
];