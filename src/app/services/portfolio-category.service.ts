import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { ErrorHandlerService } from './error-handler.service';
import { PortfolioCategory } from '../config/portfolios/models/portfolio-category';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PortfolioCategoryService {
  private client_categories_url: string;
  httpOptions: any = {};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private errorHandler: ErrorHandlerService) {

    this.client_categories_url = this.authService.endpoint("portfolio_categories");
    this.httpOptions.headers = this.authService.getHeaders();
  }

  getCategories(take: number, skip: number) {
    this.httpOptions.params = new HttpParams();
    this.httpOptions.params.append('take', take);
    this.httpOptions.params.append('skip', skip);
    
    return this.http.get<PortfolioCategory[]>(
      this.client_categories_url, 
      this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getCategories'))
    );
  }

  getCategory(id: number) {
    this.httpOptions.params = new HttpParams();
    this.httpOptions.params.set('id', id);

    return this.http.get<PortfolioCategory>(
      this.client_categories_url, 
      this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getCategory'))
    );
  }

  saveCategory(category: PortfolioCategory){
    return this.http.post<PortfolioCategory>(
      this.client_categories_url,
      category,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('saveCategory'))
      );
  }

  updateCategory(category: PortfolioCategory[]){
    return this.http.put<PortfolioCategory[]>(
      this.client_categories_url,
      category,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('updateCategory'))
      );
  }
}