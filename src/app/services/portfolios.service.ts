import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { Portfolio } from '../portfolios/models/portfolio';

@Injectable({
  providedIn: 'root'
})
export class PortfoliosService {
  private portfolios_url: string;
  httpOptions: any = {};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private errorHandler: ErrorHandlerService) {

    this.portfolios_url = this.authService.endpoint("portfolios");
    this.httpOptions.headers = this.authService.getHeaders();
  }

  getPortfolios(take: number, skip: number) {
    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('take', take);
    this.httpOptions.params.append('skip', skip);

    return this.http.get<Portfolio[]>(this.portfolios_url, this.httpOptions);
  }

  getPortfolio(id: number) {
    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('id', id);

    return this.http.get<Portfolio[]>(this.portfolios_url, this.httpOptions);
  }

  savePortfolio(portfolio: Portfolio) {

    return this.http.post<Portfolio>(
      this.portfolios_url,
      portfolio,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('savePortfolio'))
      );
  }
}

