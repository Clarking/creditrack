import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ErrorHandlerService } from './error-handler.service';
import { Client } from '../clients/models/client';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  private clients: Client[];
  private clientsUrl: string;
  private httpOptions: any = {};

  constructor(
    private http: HttpClient, 
    private authService: AuthService, 
    private errorHandler:ErrorHandlerService ) {
    
    this.clientsUrl = this.authService.endpoints["clients"];
    this.httpOptions.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authService.getToken()
    })
  }

  getClients() {
    return this.http.get<Client[]>(this.clientsUrl, this.httpOptions);
  }

  getClient(id: number) {
    return this.http.get<Client>(
      this.clientsUrl + "/" + id.toString(),
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('removeClient'))
      );
  }

  addClient(client: Client) {
    return this.http.post<Client>(
      this.clientsUrl,
      client,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('removeClient'))
      );
  }

  removeClient(id: number): Observable<{}> {

    return this.http.delete(
      this.clientsUrl + "/" + id.toString(),
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('removeClient'))
      );
  }
}
