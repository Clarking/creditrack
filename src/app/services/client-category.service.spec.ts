import { TestBed, inject } from '@angular/core/testing';

import { ClientCategoryService } from './client-category.service';

describe('ClientCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientCategoryService]
    });
  });

  it('should be created', inject([ClientCategoryService], (service: ClientCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
