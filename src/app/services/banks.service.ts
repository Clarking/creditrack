import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { ErrorHandlerService } from './error-handler.service';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Bank } from '../config/banks/models/bank';

@Injectable({
  providedIn: 'root'
})
export class BanksService {

  private bank_url: string;
  private httpOptions: any = {};

  constructor(
    private http: HttpClient, 
    private authService: AuthService, 
    private errorHandler:ErrorHandlerService ) {
    
    this.bank_url = this.authService.endpoints["banks"];
    this.httpOptions.headers = this.authService.getHeaders();
  }

  getBanks(take: number, skip: number) {
    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('take', take);
    this.httpOptions.params.append('skip', skip);

    return this.http.get<Bank[]>(this.bank_url, this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getBanks'))
    );
  }

  getBank(id: number) {
    return this.http.get<Bank>(
      this.bank_url + "/" + id.toString(),
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('getBank'))
      );
  }

  addBank(Bank: Bank) {
    return this.http.post<Bank>(
      this.bank_url,
      Bank,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('addBank'))
      );
  }

  editBank(Bank: Bank) {
    return this.http.put<Bank>(
      this.bank_url,
      Bank,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('editBank'))
      );
  }

  removeBank(id: number): Observable<{}> {

    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('id', id);
    
    return this.http.delete(
      this.bank_url,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('removeBank'))
      );
  }
}
