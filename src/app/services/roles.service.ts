import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

import { RolePermission } from '../config/roles/models/rolePermission';
import { Role } from '../config/roles/models/role';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  private roles_url: string;
  private role_permissions_url : string;
  httpOptions: any = {};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private errorHandler: ErrorHandlerService) {

    this.roles_url = this.authService.endpoint("roles");
    this.role_permissions_url = this.authService.endpoint('role_permissions');
    this.httpOptions.headers = this.authService.getHeaders();
  }

  getRoles(take: number, skip: number) {
    this.httpOptions.params = new HttpParams();
    this.httpOptions.params.append('take', take);
    this.httpOptions.params.append('skip', skip);

    return this.http.get<Role[]>(this.roles_url, this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getRoles'))
    );
  }

  getRole(id: number) {
    this.httpOptions.params = new HttpParams();
    this.httpOptions.params.set('id', id);

    return this.http.get<Role>(this.roles_url, this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getRole'))
    );
  }
  
  getPermissions(id_role:number){
 
    return this.http.get<RolePermission[]>(this.role_permissions_url +'/'+ id_role, this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getPermissions'))
    );
  }

  getBasePermissions(){
    return this.http.get<RolePermission[]>(this.role_permissions_url, this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getBasePermissions'))
    );
  }

  savePermissions(permissions:RolePermission[]){
    return this.http.post<RolePermission[]>(
      this.roles_url,
      permissions,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('savePermissions'))
      );
  }

  updatePermissions(permissions: RolePermission[]){
    return this.http.put<RolePermission[]>(
      this.roles_url,
      permissions,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('updatePermissions'))
      );
  }

  saveRole(role: Role) {

    return this.http.post<Role>(
      this.roles_url,
      role,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('saveRole'))
      );
  }
}