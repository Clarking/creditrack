import { TestBed, inject } from '@angular/core/testing';

import { PortfolioCategoryService } from './portfolio-category.service';

describe('PortfolioCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PortfolioCategoryService]
    });
  });

  it('should be created', inject([PortfolioCategoryService], (service: PortfolioCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
