import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { ErrorHandlerService } from './error-handler.service';
import { User } from '../config/users/models/user';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private users: User[];
  private users_url: string;
  private httpOptions: any = {};

  constructor(
    private http: HttpClient, 
    private authService: AuthService, 
    private errorHandler:ErrorHandlerService ) {
    
    this.users_url = this.authService.endpoints["users"];
    this.httpOptions.headers = this.authService.getHeaders();
  }

  getUsers(take: number, skip: number) {
    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('take', take);
    this.httpOptions.params.append('skip', skip);

    return this.http.get<User[]>(this.users_url, this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getUsers'))
    );
  }

  getUser(id: number) {
    return this.http.get<User>(
      this.users_url + "/" + id.toString(),
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('getUser'))
      );
  }

  saveUser(user: User) {

    return this.http.post<User>(
      this.users_url,
      user,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('addUser'))
      );
  }

  editUser(user: User) {
    return this.http.put<User>(
      this.users_url,
      user,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('editUser'))
      );
  }

  removeUser(id: number): Observable<{}> {

    this.httpOptions.params = new URLSearchParams();
    this.httpOptions.params.append('id', id);
    
    return this.http.delete(
      this.users_url,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('removeUser'))
      );
  }
}
