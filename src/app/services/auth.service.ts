import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ErrorHandlerService } from '../services/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public endpoints: any;
  private httpOptions: any;
  public redirectUrl:string;

  constructor(private http: HttpClient, private errorHandler:ErrorHandlerService) {
    this.endpoints = routes;
    this.redirectUrl = '';

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  login(username: string, password: string) {
    let token: any
    let data = JSON.stringify({ "username": username, "password": password });

    return this.http.post<any>(this.endpoints["token"], data, this.httpOptions)
      .pipe(
        tap((t: any) => {
          // login successful if there's a jwt token in the response
          if (t.key) {
            localStorage.setItem('user_token', JSON.stringify(t.key));
            delete t.key;
            localStorage.setItem('user', JSON.stringify(t));
          }
        }),
        catchError(this.errorHandler.handleError('getToken', [token]))
      )
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user_token');
    localStorage.removeItem('user');
  }

  getToken(){
    return localStorage.getItem('user_token').toString();
  }
  
  getHeaders(){
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.getToken()
    })
  }

  endpoint(name: string) {
    return this.endpoints[name];
  }
  
}

const routes = [];

routes["clients"] = 'http://creditrack.ddns.net/api/clients';
routes["portfolios"] = 'http://creditrack.ddns.net/api/portfolios';
routes["portfolios_categories"] = 'http://creditrack.ddns.net/api/portfolioCategories';
routes["roles"] = 'http://creditrack.ddns.net/api/roles';
routes["role_permissions"] = 'http://creditrack.ddns.net/api/rolePermissions';
routes["client_categories"] = 'http://creditrack.ddns.net/api/clientCategories';
routes["portfolio_categories"] = 'http://creditrack.ddns.net/api/portfolioCategories';
routes["banks"] = 'http://creditrack.ddns.net/api/banks';
routes["users"] = 'http://creditrack.ddns.net/api/users';
routes["token"] = 'http://creditrack.ddns.net/api/token';
