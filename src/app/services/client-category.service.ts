import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { ClientCategory } from '../config/client-category/models/client-category';

@Injectable({
  providedIn: 'root'
})
export class ClientCategoryService {
 
  private client_categories_url: string;
  httpOptions: any = {};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private errorHandler: ErrorHandlerService) {

    this.client_categories_url = this.authService.endpoint("client_categories");
    this.httpOptions.headers = this.authService.getHeaders();
  }

  getCategories(take: number, skip: number) {
    this.httpOptions.params = new HttpParams();
    this.httpOptions.params.append('take', take);
    this.httpOptions.params.append('skip', skip);
    
    return this.http.get<ClientCategory[]>(
      this.client_categories_url, 
      this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getCategories'))
    );
  }

  getCategory(id: number) {
    this.httpOptions.params = new HttpParams();
    this.httpOptions.params.set('id', id);

    return this.http.get<ClientCategory>(
      this.client_categories_url, 
      this.httpOptions)
    .pipe(
      catchError(this.errorHandler.handleError('getCategory'))
    );
  }

  saveCategory(category: ClientCategory){
    return this.http.post<ClientCategory>(
      this.client_categories_url,
      category,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('saveCategory'))
      );
  }

  updateCategory(category: ClientCategory[]){
    return this.http.put<ClientCategory[]>(
      this.client_categories_url,
      category,
      this.httpOptions)
      .pipe(
        catchError(this.errorHandler.handleError('updateCategory'))
      );
  }
}