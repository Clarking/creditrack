import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class ClientDetailComponent implements OnInit {

  constructor() { }

  currentOrientation = 'horizontal';
  
  ngOnInit() {
  }

}
