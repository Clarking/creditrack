import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentPromisesComponent } from './promises.component';

describe('PaymentPromisesComponent', () => {
  let component: PaymentPromisesComponent;
  let fixture: ComponentFixture<PaymentPromisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentPromisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentPromisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
