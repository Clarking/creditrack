import { Routes } from '@angular/router';
import { ClientDetailComponent } from './detail/detail.component';
import { ClientsManageComponent } from './manage/manage.component';
import { AddClientComponent } from './add/add.component';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { PaymentPromisesComponent } from './promises/promises.component';

export const clientRoutes: Routes = [
  {path: 'detail/:id', component: ClientDetailComponent, canActivate: [AuthGuard] },
  {path: 'search', component: ClientsManageComponent, canActivate: [AuthGuard] },
  {path: 'add', component: AddClientComponent, canActivate: [AuthGuard] },
  {path: 'promises', component: PaymentPromisesComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: 'search' ,  canActivate: [AuthGuard]}
];
