import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ClientCredit } from '../models/client_credit';
import { ClientContact } from '../models/client_contact';
import { HostListener } from "@angular/core";
import { Portfolio } from '../../portfolios/models/portfolio';
import { PortfoliosService } from '../../services/portfolios.service';
import { Client } from '../models/client';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddClientComponent implements OnInit {
  
  private contacts:ClientContact[];
  private contact:ClientContact;
  private client: Client;
  private viewportHeight: number;
  
  private show_contact_form:boolean = false;
  private contact_columns : [
    { prop: 'name'},
    { prop: 'address'},
    { prop: 'lastnames'},
    { prop: 'telephone'}
  ];

  private credits: ClientCredit[];
  private credit: ClientCredit;
  private show_credit_form:boolean = false;
  private credit_columns : [
    { prop: 'bank'},
    { prop: 'type'},
    { prop: 'amount'},
    { prop: 'portfolio'}
  ];
  
  private portfolios: Portfolio[];

  private credit_types = [
    { "id" : 0, "name" : "Tipo"},
    { "id" : 1, "name" : "Hipotecario"},
    { "id" : 2 , "name" : "Bancario"},
  ];

  private banks = [
    { "id" : 0, "name" : "Banco"},
    { "id" : 1, "name" : "HSBC"},
    { "id" : 2, "name" : "BBVA Bancomer"},
    { "id" : 3, "name" : "City Banamex"},
  ];

  private client_types = [
    { "id" : 0, "name" : "Tipo de cliente"},
    { "id" : 1, "name" : "HSBC"},
    { "id" : 2, "name" : "BBVA Bancomer"},
    { "id" : 3, "name" : "City Banamex"},
  ];

  private relationships = [
    { "id" : 0, "name" : "Hijo(a)"},
    { "id" : 1, "name" : "Padre"},
    { "id": 2 , "name" : "Madre"},
    { "id": 2 , "name" : "Sobrino(a)"},
    { "id": 2 , "name" : "Cuñado(a)"},
    { "id": 2 , "name" : "Amigo(a)"},
    { "id": 2 , "name" : "Vecino(a)"}
  ];

  constructor(private portfoliosService: PortfoliosService) { }

  ngOnInit() {

    this.contacts = <ClientContact[]>[];
    this.contact = <ClientContact>{};

    this.credits = <ClientCredit[]>[];
    this.credit = <ClientCredit>{};
    this.client = <Client>{};
    this.getPortfolios();
    this.onResize();
  }

  getPortfolios(){
    this.portfoliosService.getPortfolios(1000, 0).subscribe(
      (data:any) => {
        this.portfolios = data; 
      
        if(this.portfolios.length > 0){
           this.client.portfolio = this.portfolios[0].id;
        }
      }
    );
  }

  addContact(){
    this.show_contact_form = !this.show_contact_form;
  }

  saveContact() {

    this.contacts.push(this.contact);
    this.contacts = [...this.contacts];
    this.contact = <ClientContact>{};
    this.addContact();
  }

  addCredit() {
    this.show_credit_form = !this.show_credit_form;
  }

  saveCredit(){
    this.credits.push(this.credit);
    this.credits = [...this.credits]; //update table 'onPush'
    this.credit = <ClientCredit>{};
    this.addCredit();
  }

  removeCredit(event:any, value:any, row:any){
    console.log(event, row, value);
  }


  @HostListener('window:resize', ['$event'])
    onResize(event?) {
      this.viewportHeight = (window.innerHeight -60) - 42;
    }
}
