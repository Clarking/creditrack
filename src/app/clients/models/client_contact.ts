
export interface ClientContact {
    name:string,
    lastnames: string,
    address: string,
    city: string,
    telephone: number, 
    cellphone: number,
    email: string,
    relationship: any
  }