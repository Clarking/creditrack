
export interface ClientCredit {
    bank: string,
    type: string,
    portfolio : string,
    id_bank: number,
    id_credit_type : number,
    id_portfolio: number,
    amount: number
  }
  