export interface Client {
    id: number,
    name: string;
    portfolio: number;
    status: boolean;
  }