import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsMenuComponent } from './menu.component';

describe('ClientsMenuComponent', () => {
  let component: ClientsMenuComponent;
  let fixture: ComponentFixture<ClientsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
