import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-clients-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class ClientsMenuComponent implements OnInit {

  constructor() { console.log(this.is_detail);}

  @Input() is_detail: boolean;


  ngOnInit() {
  }

}
