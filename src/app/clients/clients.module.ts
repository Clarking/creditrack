import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsManageComponent } from './manage/manage.component';
import { ClientDetailComponent } from './detail/detail.component';
import { RouterModule } from '@angular/router';
import { clientRoutes } from './clients.routes';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { AddClientComponent } from './add/add.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ClientsMenuComponent } from './menu/menu.component';
import { ClientsService } from '../services/clients.service';
import { PaymentPromisesComponent } from './promises/promises.component';
import { BlacklistComponent } from './blacklist/blacklist.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    NgxDatatableModule,
    RouterModule.forChild(clientRoutes),
  ],
  declarations: [
    ClientsManageComponent, 
    ClientDetailComponent, 
    AddClientComponent, 
    ClientsMenuComponent, PaymentPromisesComponent, BlacklistComponent
  ],
  providers : [ ClientsService ]
})
export class ClientsModule { }
