import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Client } from '../models/client';
import { ClientsService } from '../../services/clients.service';
import { PortfoliosService } from '../../services/portfolios.service';
import { Portfolio } from '../../portfolios/models/portfolio';

@Component({
  selector: 'app-clients-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})

export class ClientsManageComponent implements OnInit {
  clients: Client[];
  portfolios: Portfolio[];
  private detail:boolean = false;
  
  constructor(
    private router:Router, 
    private clientsService: ClientsService,
    private portfoliosService: PortfoliosService) {

  }

  ngOnInit() {
    this.getClients();
    this.getPortfolios();
  }

  getClients(){
    this.clientsService.getClients()
    .subscribe( 
      (data:any) => this.clients = data,
             err => console.log(err.error));
  }
  
  getPortfolios(){
    this.portfoliosService.getPortfolios(1000,0)
    .subscribe( 
      (data:any) => this.portfolios = data,
             err => console.log(err.error));
  }

  editClient(event, value){
    this.router.navigate(['/clients/detail', value]);
  }
}
