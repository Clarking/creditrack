import { Component, OnInit } from '@angular/core';
import { PortfolioCategoryService } from '../../../services/portfolio-category.service';
import { PortfolioCategory } from '../models/portfolio-category';

@Component({
  selector: 'app-config-portfolios-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class PortfolioCategoryManageComponent implements OnInit {

  constructor(private portfolioCategoryService: PortfolioCategoryService) { }

  private categories:PortfolioCategory[];

  ngOnInit() {

    this.getCategories();
  }

  getCategories(){
    this.portfolioCategoryService.getCategories(1000,0)
    .subscribe( (data: any) => this.categories = data)
  }
}
