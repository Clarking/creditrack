import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioCategoryManageComponent } from './manage.component';

describe('PortfolioCategoryManageComponent', () => {
  let component: PortfolioCategoryManageComponent;
  let fixture: ComponentFixture<PortfolioCategoryManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioCategoryManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioCategoryManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
