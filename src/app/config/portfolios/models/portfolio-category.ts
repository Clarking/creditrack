export interface PortfolioCategory {
    id:number,
    name: string,
    code: string
}