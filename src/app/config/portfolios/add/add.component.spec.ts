import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioCategoryAddComponent } from './add.component';

describe('PortfolioCategoryAddComponent', () => {
  let component: PortfolioCategoryAddComponent;
  let fixture: ComponentFixture<PortfolioCategoryAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioCategoryAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioCategoryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
