import { Component, OnInit } from '@angular/core';
import { PortfolioCategoryService } from '../../../services/portfolio-category.service';
import { Router } from '@angular/router';
import { PortfolioCategory } from '../models/portfolio-category';
import { FormGroup, FormControl } from '@angular/forms';
import { formControlBinding } from '@angular/forms/src/directives/reactive_directives/form_control_directive';

@Component({
  selector: 'app-config-portfoliocategory-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class PortfolioCategoryAddComponent implements OnInit {
  private category: PortfolioCategory;
  addCategoryForm: FormGroup;

  constructor(private router: Router, private portfolioCategoryService: PortfolioCategoryService) { 
    this.category = <PortfolioCategory>{}; 

  }

  ngOnInit() {
    this.addCategoryForm = new FormGroup({
      'name' : new FormControl(null),
      'code': new FormControl(null),
      'description': new FormControl(null)
    });
  }

  addPortfolio(){
    this.portfolioCategoryService.saveCategory(this.category)
    .subscribe( (data: any) => {
      if(data.status){
        this.router.navigate(['/config/portfolios']);
      }
    } );
  }

}
