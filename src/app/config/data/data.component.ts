import { Component, OnInit, HostListener } from '@angular/core';

// https://www.papaparse.com/
import { Papa } from 'ngx-papaparse';
import { PapaParseConfig } from 'ngx-papaparse/lib/interfaces/papa-parse-config';


@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  private parsed: boolean;
  private result: any;
  private columns: any[];
  private error: string;
  private options: PapaParseConfig
  private viewportHeight: number;
  constructor(private papa: Papa) {

    this.columns = [];
    this.result = [];
    this.options = {
      complete: (r) => {

        r.data[0].forEach(element => {

          this.columns.push({
            name: element,
            prop: element
          });
        });

        r.data.forEach((row: any[], index: number) => {
          if (index == 0) {
            return;
          }

          let r = {};
          row.forEach((element: any, i: number) => {

            let value;
            if (!isNaN(element)) {
              if (element % 1 === 0) {
                value = parseInt(element);
              }
              else {
                value = parseFloat(element);
              }
            }
            else {
              value = element;
            }

            r[this.columns[i].name] = value;

          });

          this.result.push(r);
        });


        this.parsed = true;
        this.onResize();
      },
      error: (error) => {
        this.parsed = false;
        this.error = error;
      }
    }
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.result = null;
  }

  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      this.papa.parse(fileInput.target.files[0], this.options);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.viewportHeight = (window.innerHeight - 60) - 42;
  }
}
