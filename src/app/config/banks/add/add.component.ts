import { Component, OnInit } from '@angular/core';
import { Bank } from '../models/bank';
import { BanksService } from '../../../services/banks.service';
import { HostListener } from "@angular/core";
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-banks-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class BanksAddComponent implements OnInit {

  private bank: Bank;
  private addBankForm:FormGroup;
  constructor(private banksService: BanksService) {

    this.bank = <Bank>{};
   }

  private take:number;
  private skip:number
  ngOnInit() {

    this.addBankForm = new FormGroup({
      'name': new FormControl(null)
    });
    
    this.take = 10;
    this.skip = 0;

  }

  saveBank(){
    this.banksService.addBank(this.bank)
    .subscribe( (data: any) => {

      console.log(data)
    })
  }

    

}
