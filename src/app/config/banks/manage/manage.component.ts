import { Component, OnInit } from '@angular/core';
import { Bank } from '../models/bank';
import { BanksService } from '../../../services/banks.service';

@Component({
  selector: 'app-config-banks',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class BanksManageComponent implements OnInit {

  constructor(private banksService:BanksService) { }
  private banks:Bank[];
  private take:number;
  private skip:number;

  ngOnInit() {
    this.take = 10;
    this.skip = 0;

    this.getBanks();
  }

  getBanks(){
    this.banksService.getBanks(this.take, this.skip)
    .subscribe( (data:any) => this.banks = data);
  }

}
