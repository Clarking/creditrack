import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BanksManageComponent } from './manage.component';


describe('BanksManageComponent', () => {
  let component: BanksManageComponent;
  let fixture: ComponentFixture<BanksManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BanksManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BanksManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
