import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { configRoutes } from './config.routes';
import { ConfigMenuComponent } from './menu/menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UsersManageComponent } from './users/manage/manage.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataComponent } from './data/data.component';
import { UsersAddComponent } from './users/add/add.component';
import { RolesManageComponent } from './roles/manage/manage.component';
import { RolesAddComponent } from './roles/add/add.component';
import { PortfolioCategoryManageComponent } from './portfolios/manage/manage.component';
import { PortfolioCategoryAddComponent } from './portfolios/add/add.component';
import { RolesService } from '../services/roles.service';
import { UsersService } from '../services/users.service';
import { RolesEditComponent } from './roles/edit/edit.component';
import { UsersDetailComponent } from './users/detail/detail.component';
import { BanksManageComponent } from './banks/manage/manage.component';
import { BanksAddComponent } from './banks/add/add.component';
import { ClientCategoryManageComponent } from './client-category/manage/manage.component';
import { ClientCategoryAddComponent } from './client-category/add/add.component';
import { PapaParseModule } from 'ngx-papaparse';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forChild(configRoutes),
    NgbModule,
    PapaParseModule
  ],
  declarations: [ 
    ConfigMenuComponent, 
    UsersManageComponent, 
    RolesManageComponent, 
    DataComponent, 
    UsersAddComponent, 
    UsersManageComponent, 
    RolesAddComponent,
    PortfolioCategoryManageComponent,
    PortfolioCategoryAddComponent,
    RolesEditComponent,
    UsersDetailComponent,
    BanksManageComponent,
    BanksAddComponent,
    ClientCategoryManageComponent,
    ClientCategoryAddComponent ],
  providers : [UsersService, RolesService]
})
export class ConfigModule { }
