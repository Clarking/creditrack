import { Routes } from '@angular/router';
import { UsersManageComponent } from './users/manage/manage.component';
import { RolesManageComponent } from './roles/manage/manage.component';
import { DataComponent } from './data/data.component';
import { UsersAddComponent } from './users/add/add.component';
import { RolesAddComponent } from './roles/add/add.component';
import { PortfolioCategoryManageComponent } from './portfolios/manage/manage.component';
import { PortfolioCategoryAddComponent } from './portfolios/add/add.component';
import { RolesEditComponent } from './roles/edit/edit.component';
import { UsersDetailComponent } from './users/detail/detail.component';
import { BanksManageComponent } from './banks/manage/manage.component';
import { BanksAddComponent } from './banks/add/add.component';
import { ClientCategoryManageComponent } from './client-category/manage/manage.component';
import { ClientCategoryAddComponent } from './client-category/add/add.component';

export const configRoutes: Routes = [
  {path: '', redirectTo : 'users', pathMatch: 'full'},
  {path: 'users', component: UsersManageComponent},
  {path: 'users/add', component: UsersAddComponent},
  {path: 'users/edit/:id', component: UsersAddComponent},
  {path: 'users/detail/:id', component: UsersDetailComponent},
  {path: 'roles', component: RolesManageComponent},
  {path: 'roles/add', component: RolesAddComponent},
  {path: 'roles/edit/:id', component: RolesEditComponent},
  {path: 'roles/detail/:id', component: RolesAddComponent},
  {path: 'data', component: DataComponent},
  {path: 'portfolios', component: PortfolioCategoryManageComponent},
  {path: 'portfolios/add', component: PortfolioCategoryAddComponent},
  {path: 'banks', component: BanksManageComponent},
  {path: 'banks/add', component: BanksAddComponent},
  {path: 'clients', component: ClientCategoryManageComponent},
  {path: 'clients/add', component: ClientCategoryAddComponent},
];