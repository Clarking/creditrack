import { Component, OnInit } from '@angular/core';
import { RolesService } from '../../../services/roles.service';
import { Role } from '../models/role';
import { Router } from '@angular/router';

@Component({
  selector: 'app-roles-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class RolesManageComponent implements OnInit {

  constructor(private router:Router, private rolesService: RolesService) { }
  private roles: Role[];

  private take:number;
  private skip:number;

  ngOnInit() {
    this.take = 10;
    this.skip = 0;

    this.getRoles();
  }

  getRoles(){
    this.rolesService.getRoles(this.take, this.skip)
      .subscribe( ( data:any) => this.roles = data, 
      err => console.log(err.error));
  }

  viewRole(event, value:any){
    this.router.navigate(['/config/roles/detail', value]);
  }

  editRole(event, value:any){
    this.router.navigate(['config/roles/edit', value]);
  }

}
