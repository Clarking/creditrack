import { Component, OnInit } from '@angular/core';
import { Role } from '../models/role';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { RolesService } from '../../../services/roles.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class RolesAddComponent implements OnInit {
  private role:Role;
  private addRoleForm: FormGroup;
  
  constructor(private router:Router, private rolesService: RolesService) {}

  ngOnInit() {

    this.addRoleForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'code': new FormControl(null),
      'permissions' : new FormArray([], Validators.min(1)),
      'description' : new FormControl(null)
    });
  }

  getPermissions(){
    this.rolesService.getBasePermissions()
    .subscribe( (data:any) => {
      console.log(data);
      data.forEach(permission => {
        (<FormArray>this.addRoleForm.controls['permissions']).push(
          new FormControl(permission.name, Validators.required)
        )
      });
    })
  }
  
  saveRole(){
    if(this.addRoleForm.valid){

      this.rolesService.saveRole(this.addRoleForm.value)
      .subscribe( (data:any) => {
        this.router.navigate(['/config/roles']);
      });
    }
  }
}
