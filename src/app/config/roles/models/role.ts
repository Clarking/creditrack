
export interface Role {
    name:string,
    code: string,
    description: string,
    permissions: number[]
}