import { Component, OnInit } from '@angular/core';
import { RolesService } from '../../../services/roles.service';
import { Role } from '../models/role';
import { Router, ActivatedRoute } from '@angular/router';
import { RolePermission } from '../models/rolePermission';
import { FormGroup, Validators, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-roles-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class RolesEditComponent implements OnInit {
  private editRoleForm: FormGroup;

  constructor(
    private route: ActivatedRoute, 
    private router: Router, 
    private roleService: RolesService) { }

  private role:Role;
  private permissions: RolePermission[];

  ngOnInit() {
    this.editRoleForm = new FormGroup({
      'name' : new FormControl(this.role.name, Validators.required),
      'code' : new FormControl(this.role.code),
      'description': new FormControl(this.role.description),
      'permissions': new FormArray([])
    })

    this.role = <Role>{};
    this.permissions = [];  
    
    this.getRole();
    this.getPermissions();
  }

  getRole(){
    this.roleService.getRole( this.route.snapshot.params.id )
    .subscribe( (data:any) => this.role = data[0]);
  }

  getPermissions(){
    this.roleService.getBasePermissions()
    .subscribe( (basePermissions:any) => {
      
      this.roleService.getPermissions(this.route.snapshot.params.id).subscribe(
        (rolePermissions:any) => {

          let control: FormControl;
          basePermissions.forEach(
            bp => {
              rolePermissions.forEach(rp => {
                if(bp.id == rp.id_permission){
                  control = new FormControl({'checked': true})
                }
                else{
                  control = new FormControl(null);
                }
              });
            });
            
            (<FormArray>this.editRoleForm.controls['permissions']).push(control);
        }
      );
    });
  }
}
