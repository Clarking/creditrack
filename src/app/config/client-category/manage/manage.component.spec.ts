import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientCategoryManageComponent } from './manage.component';

describe('ClientCategoryManageComponent', () => {
  let component: ClientCategoryManageComponent;
  let fixture: ComponentFixture<ClientCategoryManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientCategoryManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientCategoryManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
