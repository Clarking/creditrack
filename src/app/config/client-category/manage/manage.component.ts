import { Component, OnInit } from '@angular/core';
import { ClientCategoryService } from '../../../services/client-category.service';
import { ClientCategory } from '../models/client-category';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ClientCategoryManageComponent implements OnInit {

  constructor(private clientCategoryService: ClientCategoryService) { }

  private categories:ClientCategory[];

  ngOnInit() {

    this.getCategories();
  }

  getCategories(){
    this.clientCategoryService.getCategories(1000, 0)
    .subscribe( (data:any) => this.categories = data)
  }
}
