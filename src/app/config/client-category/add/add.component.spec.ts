import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientCategoryAddComponent } from './add.component';

describe('ClientCategoryAddComponent', () => {
  let component: ClientCategoryAddComponent;
  let fixture: ComponentFixture<ClientCategoryAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientCategoryAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientCategoryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
