import { Component, OnInit } from '@angular/core';
import { ClientCategory } from '../models/client-category';
import { ClientCategoryService } from '../../../services/client-category.service';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { formControlBinding } from '@angular/forms/src/directives/reactive_directives/form_control_directive';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class ClientCategoryAddComponent implements OnInit {

  addCategoryForm: FormGroup;
  
  constructor(private router:Router, private clientCategoryService: ClientCategoryService) { }

  ngOnInit() {
    this.addCategoryForm = new FormGroup({
      'name': new FormControl(null, Validators.required)
    });
  }

  saveCategory(){
    
    if(this.addCategoryForm.valid){
      this.clientCategoryService.saveCategory(this.addCategoryForm.value)
      .subscribe( (data:any) => {
        if(data.status) {
          this.router.navigate(['/config/clients']);
        }
      })
    }
  }
}
