
export interface ClientCategory {
    id: number,
    name:string,
    description: string
} 