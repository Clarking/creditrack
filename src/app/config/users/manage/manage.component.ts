import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { UsersService } from '../../../services/users.service';
import { User } from '../models/user';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class UsersManageComponent implements OnInit {

  constructor(private router: Router, private usersService: UsersService) { }

  private users: User[];
  private take: number;
  private skip: number;

  ngOnInit() {
    this.take = 10;
    this.skip = 0;

    this.getUsers();
  }

  getUsers() {
    this.usersService.getUsers(this.take, this.skip)
      .subscribe((data: any) => this.users = data);
  }

  editUser(event, value) {
    this.router.navigate(['/config/users/edit', value]);
  }

  viewUser(event, value){
    this.router.navigate(['/config/users/detail', value]);
  }

}