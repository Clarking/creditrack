
export interface User {
    name:string,
    lastnames: string,
    email: string,
    username: string,
    lastLogin: Date,
    fullname: string,
    role: number
}