import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UsersService } from '../../../services/users.service';
import { RolesService } from '../../../services/roles.service';
import { Role } from '../../roles/models/role';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class UsersAddComponent implements OnInit {
  
  private roles: Role[];
  private addUserForm: FormGroup;
  constructor(private router: Router, private usersService:UsersService, private rolesService: RolesService) { 
    
  }

  ngOnInit() {
    this.addUserForm = new FormGroup({
      'name' : new FormControl(null, Validators.required),
      'lastnames' : new FormControl(null, Validators.required),
      'username' :new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'role': new FormControl(null, [Validators.required, Validators.min(1)])
    });
    this.addUserForm.controls['role'].setValue(0, {onlySelf: true});
    this.roles = [];
    this.getRoles();
  }

  getRoles(){
    this.rolesService.getRoles(1000,0)
    .subscribe( (data:any) => this.roles = data);
  }

  saveUser(){

    if(this.addUserForm.valid){

      this.usersService.saveUser(this.addUserForm.value)
      .subscribe( (data:any) => {
  
        if( data.status){
          this.router.navigate(['/config/users'])
        }
      });
    }
  }

  nameValid(){
    return !this.addUserForm.get('name').valid && this.addUserForm.get('name').touched;
  }

  emailValid(){
    return !this.addUserForm.get('email').valid && this.addUserForm.get('email').touched;
  }

  usernameValid(){
    return !this.addUserForm.get('username').valid && this.addUserForm.get('username').touched;
  }

  roleValid(){
    return !this.addUserForm.get('role').valid  && this.addUserForm.get('role').touched;
  }
}
