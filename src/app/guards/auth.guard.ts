import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private authService:AuthService, private router: Router) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
      let url: string = state.url;
  
      // logged in so return true
      return this.checkLogin(url);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot, 
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }

  checkLogin(url:string) {
    if (localStorage.getItem('user_token')) {
      return true;
    } 

    // Store the attempted URL for redirecting
    this.authService.redirectUrl = url;

    // Navigate to the login page 
    this.router.navigate(['/login']);
    return false;
  }
}
