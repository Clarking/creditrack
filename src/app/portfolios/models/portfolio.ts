export interface Portfolio {
    id: number,
    name:string,
    description: string,
    code: string,
    category: number,
    balance: number,
}