import { Component, OnInit } from '@angular/core';
import { Portfolio } from '../models/portfolio';

import { PortfoliosService } from '../../services/portfolios.service';
import { Router } from '@angular/router';
import { PortfolioCategoryService } from '../../services/portfolio-category.service';
import { PortfolioCategory } from '../../config/portfolios/models/portfolio-category';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class PortfolioAddComponent implements OnInit {
  
  private portfolio: Portfolio;
  private portfolio_categories: PortfolioCategory[];
  private take: number;
  private skip: number;

  constructor(
    private router: Router, 
    private portfolioService: PortfoliosService,
    private portfolioCategoryService: PortfolioCategoryService
  ) { }

  ngOnInit() {
    this.take= 10;
    this.skip = 0
    this.portfolio = <Portfolio>{};
    this.portfolio_categories = [];
    this.getCategories();
  }

  getCategories(){
    this.portfolioCategoryService.getCategories(this.take, this.skip)
    .subscribe( (data:any) => this.portfolio_categories = data);
  }

  addPortfolio() {
    this.portfolioService.savePortfolio(this.portfolio)
    .subscribe( (data:any) => {
      if(data.status) {
        this.router.navigate(['/portfolios']);
      }
    });
  }
}
