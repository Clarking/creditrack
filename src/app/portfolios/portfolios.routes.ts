import { Routes } from '@angular/router';
import { PortfolioManageComponent } from './manage/manage.component';
import { PortfolioAddComponent } from './add/add.component';
import { PortfolioDetailComponent } from './detail/detail.component';
import { AuthGuard } from '../guards/auth.guard';

export const portfolioRoutes: Routes = [
  {path: 'add', component: PortfolioAddComponent, canActivate: [AuthGuard]},
  {path: 'search', component: PortfolioManageComponent, canActivate: [AuthGuard]},
  {path: 'detail/:id', component: PortfolioDetailComponent, canActivate: [AuthGuard]},
  {path: 'edit/:id', component: PortfolioAddComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: 'search', pathMatch: 'full'}
];
