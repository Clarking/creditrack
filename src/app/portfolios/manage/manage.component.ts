import { Component, OnInit } from '@angular/core';
import { Portfolio } from  '../models/portfolio';
import { PortfoliosService } from '../../services/portfolios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class PortfolioManageComponent implements OnInit {
  private portfolio:Portfolio;
  private portfolios: Portfolio[];
  private take = 15;
  private skip = 0;
  constructor(private router: Router, private portfolioService: PortfoliosService) { }

  ngOnInit() {
    this.portfolio = <Portfolio>{};
    
    this.portfolioService.getPortfolios(this.take, this.skip)
    .subscribe( 
      (data: any) => this.portfolios =data,
              err => console.log(err.error));
  }

  editPortfolio(event, value){
    this.router.navigate(['/portfolios/edit', value]);
  }

  viewPortfolio(event, value){
    this.router.navigate(['/portfolios/detail', value]);
  }
}
