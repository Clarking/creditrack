import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortfolioManageComponent } from './manage/manage.component';
import { PortfolioAddComponent } from './add/add.component';
import { portfolioRoutes } from './portfolios.routes';

import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PortfoliosMenuComponent } from './menu/menu.component';
import { PortfoliosService } from '../services/portfolios.service';
import { PortfolioDetailComponent } from './detail/detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    RouterModule.forChild(portfolioRoutes),
  ],
  declarations: [
    PortfolioManageComponent, 
    PortfolioAddComponent, 
    PortfoliosMenuComponent, 
    PortfolioDetailComponent
  ],
  providers: [ PortfoliosService ]
})
export class PortfoliosModule { }




